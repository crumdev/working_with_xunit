using System;
using Xunit;

namespace GameEngine.Tests
{
    public class PlayerCharacterShould : IDisposable
    {
        private readonly PlayerCharacter _sut;

        public PlayerCharacterShould()
        {
            _sut = new PlayerCharacter();
        }

        public void Dispose()
        {
            // Put Centrallized cleanup here
        }

        [Fact]
        public void BeInexperiencedWhenNew()
        {
            

            Assert.True(_sut.IsNoob) ;
        }

        [Fact]
        public void CalculateFullName()
        {
            _sut.FirstName = "Nathan";
            _sut.LastName = "Crum";

            Assert.Equal("Nathan Crum", _sut.FullName);
        }

        [Fact]
        public void StartsWithDefaultHealth()
        {
            

            Assert.Equal(100, _sut.Health);
        }

        [Fact]
        public void IncreaseHealthAfterSleeping()
        {
            
            
            _sut.Sleep(); //Expect increase between 1 to 100

            Assert.InRange(_sut.Health,101,200);
        }

        [Fact]
        public void NotHaveNickNameByDefault()
        {
            
            Assert.Null(_sut.Nickname);
        }

        [Fact]
        public void HaveALongBow()
        {
            Assert.Contains("Long Bow",_sut.Weapons);
        }

        [Fact]
        public void HaveAtLeastOneKindOfSword()
        {
            Assert.Contains(_sut.Weapons, weapon => weapon.Contains("Sword") );
        }

        [Fact]
        public void HaveAllExpectedWeapons()
        {
            var expectedWeapons = new[]{
                "Long Bow",
                "Short Bow",
                "Short Sword"
            };

            Assert.Equal(expectedWeapons, _sut.Weapons);
            Assert.Contains(_sut.Weapons, weapon => weapon.Contains("Sword") );
        }

        [Fact]
        public void RaiseSleptEvent()
        {
            Assert.Raises<EventArgs>(
                handler => _sut.PlayerSlept += handler,
                handler => _sut.PlayerSlept -= handler,
                () => _sut.Sleep());
        }

        [Fact]
        public void RaisePropertyChangedEvent()
        {
            Assert.PropertyChanged(_sut, "Health", () => _sut.TakeDamage(10));
        }
        
    }
}
